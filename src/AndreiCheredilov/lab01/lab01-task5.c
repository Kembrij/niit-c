/*Написать программу, которая принимает строку от пользователя и
выводит её на экран, выравнивая по центру*/

#include <stdio.h>
#include <string.h>
#define width 80

int main()
{
    char arr[80];
    int length;

    printf("Please enter random string:\n");
    fgets(arr, 80, stdin);

    length = strlen(arr);

    for (int i = 0; i <= (width - length) / 2; i++)
    {
        fputs(" ", stdout);
    }
    fputs(arr, stdout);
    // printf("%*s", 40 + strlen(arr) / 2, arr, 40 - strlen(arr) / 2,"");

    return 0;
}
