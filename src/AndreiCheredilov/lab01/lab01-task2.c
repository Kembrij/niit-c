/*Написать программу, которая запрашивает текущее время в фор-
мате ЧЧ:ММ:СС, а затем выводит приветствие в зависимости от
указанного времени ("Доброе утро "Добрый день"и т.д.)*/

#include <stdio.h>
int main()
{
    int hh, mm, ss;

    printf("Please enter current time in format hh:mm:ss\n");
    scanf("%d%d%d", &hh, &mm, &ss);
    printf("Current time is %d\:%d\:%d\n", hh, mm, ss);

    if (hh >= 6 && hh < 10)
        printf("Good morning!\n");
    else if (hh >= 10 && hh < 18)
        printf("Good day!\n");
    else if (hh >= 18 && hh < 22)
        printf("Good evening!\n");
    else if (hh >= 22 && hh < 24)
        printf("Good night!\n");
    else if (hh >= 0 && hh < 6)
        printf("Good night!\n");

    return 0;
}
