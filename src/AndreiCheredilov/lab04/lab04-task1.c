/*
�������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ROWS 5
#define MAXLEN 20

int main()
{
    char str[ROWS][MAXLEN]={0};
	char *pointer[ROWS];
	char *temp;
	int i=0,j=0;

    printf("Please enter %d strings to sort\n", ROWS);

    for(int i = 0; i < ROWS; i++)
    pointer[i] = *((str+i));

	while(i<ROWS && *fgets(*(str+i),MAXLEN,stdin)!='\n')
    i++;

    for(i = 0; i < ROWS; ++i)
    {
        for(j = i; j < ROWS; ++j)
        {
            if (strlen(pointer[i]) > strlen(pointer[j]))
            {
				temp=pointer[i];
				pointer[i]=pointer[j];
				pointer[j]=temp;
            }

        }
    }


   printf("Sorted List :\n");
   for (int i = 0; i < ROWS; i++)
      printf(pointer[i]);

    return 0;
}
