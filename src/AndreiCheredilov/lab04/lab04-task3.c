/*
�������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������:
���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define M 256
#define MAXLEN 20

int main()
{
    char str[M];

    printf("Please enter random string here: \n");
    fgets(str, M, stdin);

    int lenght = strlen(str) - 2;
    char *firstchar = str;
    char *lastchar = str + lenght;

    while (firstchar < lastchar)
    {
        if (*firstchar != *lastchar)
        {
            printf("The str is not a palindrome!\n");
            return 0;
        }
        *firstchar++;
        *lastchar--;
    }

    printf("The str is a palindrome!\n");
    return 0;
}
