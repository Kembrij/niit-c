/*�������� ��������� ������� �����. ��������� ���������� ����� � �����-
���� �� 1 �� 100 � ������������ ������ ������� ��� �� ���������� ����������
�������.
���������:
������������ ������ �����, � ��������� ������������ ���: �������, �����-
��, �������!�.*/

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int num;
    setlocale(LC_ALL, "Rus");

    srand(time(NULL));
    int r = rand() % 10;

    printf("���������� ������� �����-������ ����� �� 1 �� 100:\n");
    if (scanf("%d", &num) != 1)
        printf("������������ ��� ������\n");
    else if (num <= 0 || num > 100)
        printf("�������� �������� ��������\n");

    while (1)
    {
        if (num > r)
        {
            printf("���� ����� ������ ���������������!\n");
            scanf("%d", &num);
        }
        else if (num < r)
        {
            printf("���� ����� ������ ���������������!\n");
            scanf("%d", &num);
        }
        else if (num == r)
        {
            printf("������!\n");
            break;
        }
    }
    return 0;
}
