/*
Написать программу, очищающую строку от лишних пробелов. Лишними счи-
таются пробелы в начале строки, в конце строки и пробелы между словами,
если их количество больше 1.
Замечание:
В данной программе запрещёно создавать дополнительные массивы, то есть
необходимо стремиться к экономии памяти. Время выполнения программы зна-
чения не имеет.
*/

#include <stdio.h>
#include <string.h>

int main()
{
    char input[256];
    int s = 0, i = 0, j = 0, k = 0;
    
    printf("Input string: ");
    fgets(input, 256, stdin);
    
    //remove spaces at start
    while(input[i++] == ' ')
        ;
    while(input[j + i - 1] != '\0') //<=> sprintf(input, "%s", input + i - 1);
    {
        input[j] = input[j + i - 1];
        j++;
    }
    input[j] = '\0';
    
    //remove spaces at end
    i = strlen(input) - 1;
    while(input[i] == ' ' || input[i] == '\n')
        i--;
    input[i + 1] = '\0';

    //remove spaces in middle positions
    i = j = 0;
    while(input[i++] != '\0')
    {
        while(input[j++] == ' ')
            s++;
        if(s >= 1) // <=> sprintf(input, "%.*s%s", i, input, input + i - 1 + s);
        {
            k = i;
            while(input[k + s - 1] != '\0')
            {
                input[k] = input[k + s - 1];
                k++;
            }
            input[k] = '\0';
                
        }
            
        s = 0;
        j = i;
    }
    printf("%s\n", input);
    return 0;
    
}